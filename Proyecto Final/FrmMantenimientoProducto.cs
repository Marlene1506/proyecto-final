﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace FactuxD
{
    public partial class FrmMantenimientoProducto : Mantenimiento
    {
        public FrmMantenimientoProducto()
        {
            InitializeComponent();
        }

        public override Boolean Guardar()
        {
            if(Utilidades.ValidarFormulario(this,errorProvider1)==false)
            {
                try
                {
                    string cmd = string.Format("EXEC ActualizarArticulos'{0}','{1}','{2}'", txtidPro.Text.Trim(), txtNomPro.Text.Trim(), txtPrecio.Text.Trim());
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Se ha guardado correctamente.");
                    return true;
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error " + error.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public override void Eliminar()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarArticulo'{0}'", txtidPro.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha eliminado correctamente.");
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error " + error.Message);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void txtidPro_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void FrmMantenimientoProducto_Load(object sender, EventArgs e)
        {

        }
    }
}
