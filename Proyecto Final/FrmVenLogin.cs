﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MiLibreria;

namespace FactuxD
{
    public partial class FrmVenLogin : FormBase
    {
        public FrmVenLogin()
        {
            InitializeComponent();
        }

        public static String Codigo = "";

        private void btnIniciar_Click(object sender, EventArgs e)
        {

            try
            {
                string CMD = string.Format("select * FROM Usuarios Where account='{0}'AND password='{1}'", txtNomAcc.Text.Trim(), txtPass.Text.Trim());

                DataSet ds = Utilidades.Ejecutar(CMD);

                Codigo = ds.Tables[0].Rows[0]["id_usuario"].ToString().Trim();

                string cuenta = ds.Tables[0].Rows[0]["account"].ToString().Trim();

                string contra = ds.Tables[0].Rows[0]["password"].ToString().Trim();

                if (cuenta == txtNomAcc.Text.Trim() && contra == txtPass.Text.Trim())
                {
                    if (Convert.ToBoolean(ds.Tables[0].Rows[0]["Status_admin"]) == true)
                    {
                        FrmVentanaAdmin VenAd = new FrmVentanaAdmin();
                        this.Hide();
                        VenAd.Show();
                    }
                    else
                    {
                        FrmVentanaUser VenUs = new FrmVentanaUser();
                        this.Hide();
                        VenUs.Show();
                    }
                }
            }
            catch (Exception error)
            {
                MessageBox.Show("Usuario o cuentraseña incorrecta "+error.Message);
            }
        }
        private void txtNomAcc_TextChanged(object sender, EventArgs e)
        {

        }

        private void venLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void venLogin_Load(object sender, EventArgs e)
        {

        }

        private void txtNomAcc_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            FrmRegistrar frmRegistrar = new FrmRegistrar();
            this.Hide();
            frmRegistrar.Show();
        }
    }
}
