﻿namespace FactuxD
{
    partial class FrmConsultarProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // btnSeleccionar
            // 
            this.btnSeleccionar.Location = new System.Drawing.Point(12, 373);
            this.btnSeleccionar.Size = new System.Drawing.Size(111, 23);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(330, 15);
            this.button3.Size = new System.Drawing.Size(111, 23);
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(417, 373);
            this.btnSalir.Size = new System.Drawing.Size(111, 23);
            // 
            // FrmConsultarProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FactuxD.Properties.Resources.Fondo_2;
            this.ClientSize = new System.Drawing.Size(557, 423);
            this.Name = "FrmConsultarProductos";
            this.Load += new System.EventHandler(this.ConsultarProductos_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}