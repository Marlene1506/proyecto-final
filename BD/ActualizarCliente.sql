USE [Administracion]
GO
/****** Object:  StoredProcedure [dbo].[ActualizarCliente]    Script Date: 04/02/2019 10:56:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[ActualizarCliente]

@Id_cli int,@Nom_cli varchar(50),@Ape_cli varchar (50)

as

--Actializa Clientes

if not exists (Select id_clientes FROM Cliente where id_clientes=@Id_cli)
insert into cliente (id_clientes,Nom_cli,Ape_cli) values (@Id_cli,@Nom_cli,@Ape_cli)
else 
update cliente set id_clientes=@Id_cli,Nom_cli=@Nom_cli,Ape_cli=@Ape_cli where id_clientes=@Id_cli